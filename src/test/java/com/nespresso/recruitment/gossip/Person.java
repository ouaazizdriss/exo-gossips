/**
 * 
 */
package com.nespresso.recruitment.gossip;

import java.util.ArrayList;
import java.util.List;

/**
 * @author driss
 *
 */
public class Person {

	
	/**
	 * Titre de la personne.
	 */
	private String title;
	
	/**
	 * Nom de la personne.
	 */
	private String name;
	
	/**
	 * List des messages a passer.
	 */
	private List<String> messages; 
	
	/**
	 * from
	 */
	private String from;
	
	/**
	 * to
	 */
	private String to;
	
	
	/**
	 * @param pName 
	 * @param pTitle 
	 * 
	 */
	public Person(String pTitle, String pName) {
		name = pName;
		title = pTitle;
		messages = new ArrayList<>();
	}


	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}



	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}



	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the messages
	 */
	public List<String> getMessages() {
		return messages;
	}


	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}


	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}


	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}


	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}


	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}


}
