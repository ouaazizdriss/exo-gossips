/**
 * 
 */
package com.nespresso.recruitment.gossip;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author driss
 *
 */
public class Gossips {
	
	/**
	 * Liste des personnes.
	 */
	private List<Person> people = new LinkedList<Person>();
	
	private Person toEditNext;
	
	private String messageToSay;
	
	private List<Person> wantToSay = new ArrayList<Person>();
	
	private static int level = 0;
	
	/**
	 * Constructeur.
	 */
	public Gossips(String ... names) {
		
		for(final String p: names){
			String pTitle = p.split(" ")[0];
			String pName = p.split(" ")[1];
			Person person = new Person(pTitle, pName);
			people.add(person);
		}
		
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public Gossips from(final String name) {
		toEditNext = this.getPersonByName(name);
		return this;
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public Gossips to(final String name) {
		
		Person toPerson = this.getPersonByName(name);
		level = people.indexOf(toPerson);
		
		//from = toEditNext
		if(toEditNext!=null){
			toPerson.setFrom(toEditNext.getName());
			toEditNext.setTo(toPerson.getName());
			toEditNext = null;
		}
		
		//TODO add condition on title: to save messages or not
		toPerson.getMessages().add(messageToSay);
		wantToSay.add(toPerson);
		return this;
	}

	/**
	 * Say a message.
	 * 
	 * @param message
	 * @return
	 */
	public Gossips say(String message) {
		messageToSay = message;
		return this;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public String ask(String name) {
		Person p = this.getPersonByName(name);
		//TODO add test on title of person
		return p.getMessages().size()>0?p.getMessages().get(0):"";
	}

	/**
	 * Spread the messages.
	 */
	public void spread() {
		
			//TODO add test on title
			Person p = people.get(level);
			level++;
			if(p.getTo()!=null && this.getPersonByName(p.getTo())!=null && p.getMessages().size()>0){
				this.getPersonByName(p.getTo()).getMessages().clear();
				this.getPersonByName(p.getTo()).getMessages().add(p.getMessages().get(0));
				messageToSay =p.getMessages().get(0); 
				p.getMessages().clear();
			}
	}
	
	/**
	 * Retourne une personne par son nom.
	 * 
	 * @param name
	 * @return
	 */
	private Person getPersonByName(final String name){
		for(Person person:people){
			if(name.equals(person.getName())){
				return person;
			}
		}
		return null;
	}
	

}
